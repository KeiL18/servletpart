package com.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.connessione.ConnettoreDB;
import com.models.Ordine;
import com.models.Store;

public class StoreDao implements Dao<Store>{

	@Override
	public Store getById(int id) throws SQLException {
Connection conn = ConnettoreDB.getIstanza().getConnessione();
        
       	String query = "SELECT idStore,nome,pIva,nomeutente,passwordStore,emailStore FROM store WHERE idStore = ?";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setInt(1, id);
       	
       	ResultSet risultato = ps.executeQuery();
       	risultato.next();

   		Store temp = new Store();
   		temp.setIdStore(risultato.getInt(1));
   		temp.setNome(risultato.getString(2));
   		temp.setpIva(risultato.getString(3));
   		temp.setNomeUtente(risultato.getString(4));
   		temp.setPasswordStore(risultato.getString(4));
   		temp.setEmailStore(risultato.getString(6));
   		
   		String querySelect = "SELECT idProdotto,prodotto.nome,codice,descrizione,prezzo,quantita FROM store JOIN prodotto ON store.idStore=prodotto.refStore WHERE idStore=?";
       	PreparedStatement ps1 = (PreparedStatement) conn.prepareStatement(query);
       	ps1.setInt(1, id);
       	ResultSet ris=ps1.executeQuery();
       	ArrayList<Prodotto> elenco1=new ArrayList<Prodotto>();
       	
       	while(ris.next()) {
       		int id1=ris.getInt(1);
       		String nome=ris.getString(2);
       		String codice=ris.getString(3);
       		String descrizione=ris.getString(4);
       		Double prezzo=ris.getDouble(5);
       		int quantita=ris.getInt(6);
       		
       		Prodotto temp1=new Prodotto(id1,nome,codice,descrizione,prezzo,quantita);
       		elenco1.add(temp1);
       		
       	}
       	temp.setElenco(elenco1);
       	
       	return temp;
	}

	@Override
	public ArrayList<Store> getAll() throws SQLException {
Connection conn = ConnettoreDB.getIstanza().getConnessione();
ArrayList<Store> elencoStore=new ArrayList<Store>();
        
       	String query = "SELECT idStore,nome,pIva,nomeutente,passwordStore,emailStore FROM store";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	
       	ResultSet risultato = ps.executeQuery();
       	while(risultato.next()) {

   		Store temp = new Store();
   		temp.setIdStore(risultato.getInt(1));
   		temp.setNome(risultato.getString(2));
   		temp.setpIva(risultato.getString(3));
   		temp.setNomeUtente(risultato.getString(4));
   		temp.setPasswordStore(risultato.getString(4));
   		temp.setEmailStore(risultato.getString(6));
   		
   		String querySelect = "SELECT idProdotto,prodotto.nome,codice,descrizione,prezzo,quantita FROM store JOIN prodotto ON store.idStore=prodotto.refStore WHERE idStore=?";
       	PreparedStatement ps1 = (PreparedStatement) conn.prepareStatement(query);
       	ps1.setInt(1, temp.getIdStore());
       	ResultSet ris=ps1.executeQuery();
       	ArrayList<Prodotto> elenco1=new ArrayList<Prodotto>();
       	
       	while(ris.next()) {
       		int id1=ris.getInt(1);
       		String nome=ris.getString(2);
       		String codice=ris.getString(3);
       		String descrizione=ris.getString(4);
       		Double prezzo=ris.getDouble(5);
       		int quantita=ris.getInt(6);
       		
       		Prodotto temp1=new Prodotto(id1,nome,codice,descrizione,prezzo,quantita);
       		elenco1.add(temp1);
       		
       	}
       	temp.setElenco(elenco1);
       	}
       	
       	return elencoStore;
	}

	@Override
	public void insert(Store t) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();

   		String query = "INSERT INTO store (nome,pIva,nomeutente,passwordStore,emailStore) VALUES (?,?,?,?)";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
       	ps.setString(1, t.getNome());
       	ps.setString(2, t.getpIva());
       	ps.setString(3, t.getNomeUtente());
       	ps.setString(4, t.getPasswordStore());
       	ps.setString(5, t.getEmailStore());
       	
       	ps.executeUpdate();
       	ResultSet risultato = ps.getGeneratedKeys();
       	risultato.next();
   		
       	t.setIdStore(risultato.getInt(1));
		
	}

	@Override
	public boolean delete(Store t) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
   		String query = "DELETE FROM store WHERE idStore = ?";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setInt(1, t.getIdStore());
       	
       	int affRows = ps.executeUpdate();
       	if(affRows > 0)
       		return true;
       	
   		return false;
	}

	@Override
	public Store update(Store t) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
   		String query = "UPDATE store SET "
   				+ "nome = ?, "
   				+ "pIva=?, "
   				+ "nomeutente=?, "
   				+ "passwordStore=?, "
   				+ "emailStore=? "
   				+ "WHERE idStore = ?";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setString(1, t.getNome());
       	ps.setString(2, t.getpIva());
       	ps.setString(3, t.getNomeUtente());
       	ps.setString(4, t.getPasswordStore());
       	ps.setString(5, t.getEmailStore());
       	
       	int affRows = ps.executeUpdate();
       	if(affRows > 0)
       		return getById(t.getIdStore());
       	
   		return null;
	}

}
