package com.models;

public class Store {
	int idStore;
	String nome;
	String pIva;
	String nomeUtente;
	String passwordStore;
	String emailStore;
	ArrayList<Prodotto> elenco;
	
	
	
	
	public ArrayList<Prodotto> getElenco() {
		return elenco;
	}
	public void setElenco(ArrayList<Prodotto> elenco) {
		this.elenco = elenco;
	}
	public int getIdStore() {
		return idStore;
	}
	public void setIdStore(int idStore) {
		this.idStore = idStore;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getpIva() {
		return pIva;
	}
	public void setpIva(String pIva) {
		this.pIva = pIva;
	}
	public String getNomeUtente() {
		return nomeUtente;
	}
	public void setNomeUtente(String nomeUtente) {
		this.nomeUtente = nomeUtente;
	}
	public String getPasswordStore() {
		return passwordStore;
	}
	public void setPasswordStore(String passwordStore) {
		this.passwordStore = passwordStore;
	}
	public String getEmailStore() {
		return emailStore;
	}
	public void setEmailStore(String emailStore) {
		this.emailStore = emailStore;
	}
	
	
	
}
