DROP DATABASE IF EXISTS eCommerceDelivery;
CREATE DATABASE eCommerceDelivery;
USE eCommerceDelivery;


-- Per noi prodotti uguali hanno lo stesso codice. Abbiamo un attributo quantità per tenere
-- conto di questo.




CREATE TABLE utente(
idUtente INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
nome VARCHAR(20) NOT NULL,
cognome VARCHAR(20)  NOT NULL,
email VARCHAR(20) UNIQUE,
nickname VARCHAR(20) UNIQUE NOT NULL,
passw VARCHAR(20) NOT NULL,
ruolo VARCHAR(10) CONSTRAINT CHECK (ruolo="ADMIN" OR ruolo="BASE")
);

CREATE TABLE store(
idStore INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
nome VARCHAR(20) NOT NULL,
pIva VARCHAR(20) UNIQUE NOT NULL,
nomeutente VARCHAR(20),
passwordStore VARCHAR(20),
emailStore VARCHAR(20)
);

CREATE TABLE prodotto(
idProdotto INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
nome VARCHAR(20),
codice VARCHAR(20),
descrizione VARCHAR(150),
prezzo INTEGER,
quantita INTEGER,
refStore INTEGER NOT NULL,
FOREIGN KEY (refStore) REFERENCES Store(idStore)
);

-- Mettiamo l'indirizzo di spedizione all'interno dell'ordine, in questo modo l'utente può cambiare
-- indirizzo ma gli ordini già spediti conservano l'indirizzo inserito in precedenza.

CREATE TABLE ordine(
idOrdine INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
codice VARCHAR(20) NOT NULL UNIQUE,
indirizzo VARCHAR(20) NOT NULL,
refUtente INTEGER NOT NULL,
FOREIGN KEY (refUtente) REFERENCES utente(idUtente) ON DELETE CASCADE
);

-- Tabella di appoggio per la relazione molti a molti fra prodotti ed ordini

CREATE TABLE ProdottoOrdine(
refProdotto INTEGER NOT NULL,
refOrdine INTEGER NOT NULL,
FOREIGN KEY (refProdotto) REFERENCES Prodotto(idProdotto),
FOREIGN KEY (refOrdine) REFERENCES Ordine(idOrdine)
);


INSERT INTO utente (nome,cognome,nickname,email,passw,ruolo) VALUES
("Andrea","Luca","Andrew97","andreLuce@gmail.com","123","ADMIN"),
("Meryeme","Aboulhorma","Meryeme","meryeme@gmail.com","123","BASE"),
("Federico","Chiaffedo","Fc","federico@gmail.com","123","BASE"),
("Giovanni","Pace","Gpace","giovanni@gmail.com","123","BASE");

INSERT INTO store (nome,pIva,nomeutente,passwordStore,emailStore) VALUES
("Store informatica","ABC123","Inf404","123","email@email.com"),
("Store cucina","ABC1234","Cuc405","123","email@email.com"),
("Store arredo","ABC1235","Arr406","123","email@email.com"),
("Store ufficio","ABC1236","Uff407","123","email@email.com"),
("Store musica","ABC1237","Mus408","123","email@email.com");

INSERT INTO ordine (codice,indirizzo,refUtente) VALUES
("ABC","Corso Bho","1"),
("ABCD","Corso Rosso","2"),
("ABCE","Corso Verde","2"),
("ABCF","Corso Blu","3"),
("ABCG","Corso Giallo","3"),
("ABCH","Corso Chissa","4");


INSERT INTO prodotto(nome,codice,descrizione,prezzo,quantita,refStore) VALUES
("Penna","PENN","descrizione penna",0.5,100,4),
("Computer","PC","descrizione pc",400,45,1),
("Pasta","PAST","descrizione pasta",1.5,200,2),
("Sedia","SED","descrizione sedia",80,100,4),
("Carta","CART","descrizione carta",0.5,100,4);

INSERT INTO ProdottoOrdine (refProdotto,refOrdine) VALUES
(1,1),
(2,1),
(3,2),
(4,3),
(1,6);


SELECT * FROM ordine;

-- tabella per la relazione 1:molti fra utente ed ordini

SELECT * FROM utente
         JOIN ordine ON utente.idUtente=ordine.refUtente;

--  tabella di join per la relazione store-prodotti
SELECT * FROM store
         JOIN prodotto ON store.idStore=prodotto.refStore;

-- tabella di join per prodotti e ordini

SELECT * FROM prodotto
        JOIN ProdottoOrdine ON prodotto.idProdotto=ProdottoOrdine.refProdotto
        JOIN ordine ON ProdottoOrdine.refOrdine=ordine.idOrdine;
